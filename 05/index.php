<?php
  date_default_timezone_set('America/Sao_Paulo');
  require_once("head.php");

  if (isset($_GET['deslogar'])):
    unset($_COOKIE);
    setcookie('form-contato', '', time() - 3600 * 1000);
    header("location: login.php");
  endif;

  if (!isset($_COOKIE['form-contato'])):
    header("location: login.php");
  endif;

  echo "<a href='?deslogar'>Deslogar</a>";
  if (isset($_POST['excluir'])) :
    require_once("excluir.php");
    require_once("imprime-tabela.php");

  elseif (isset($_POST['enviar']) || isset($_POST['atualizar'])) :
    require_once("gravar.php");
    require_once("imprime-tabela.php");

  else :
    require_once("imprime-form.php");

  endif;
  require_once("eof.php");
?>
