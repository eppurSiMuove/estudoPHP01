<?php
  try {
    $conexao = new PDO("mysql:host=localhost;dbname=estudos01", "Admin", "123456");
    $str = date('Y-m-d H:i:s', time()) . " Conexão com banco de dados efetuada com sucesso.\n";
    $fb_events = fopen("events.log", "a+");
    fwrite($fb_events, $str, strlen($str));
    fclose($fb_events);

    $sql = "DELETE FROM dados WHERE id = {$_POST['excluir']}";
    $conexao->query($sql);
    $str = date('Y-m-d H:i:s', time()) . " Exclusão de dados efetuada com sucesso.\n";
    $fb_events = fopen("events.log", "a+");
    fwrite($fb_events, $str, strlen($str));
    fclose($fb_events);

  } catch (PDOException $err) {
    $str = date('Y-m-d H:i:s', time()) . " Não foi possível realizar a operação no banco de dados.\n";
    $err = date('Y-m-d H:i:s', time()) . " " . $err . "\n";
    $fb_errors = fopen("errors.log", "a+");
    fwrite($fb_errors, $str, strlen($str));
    fwrite($fb_errors, $err, strlen($err));
    fclose($fb_errors);
    die("$str <br>");
  }
?>
