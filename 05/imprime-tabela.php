<table>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nome</th>
      <th scope="col">E-mail</th>
      <th scope="col">Mensagem</th>
      <th scope="col">Data</th>
      <th colspan="2">Ações</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $conexao = new mysqli("localhost", "Admin", "123456", "estudos01");
      if ($conexao->connect_error) {
        echo "something went wrong x.x";
      } else {
        echo "successfully connected to MariaDB server.<br><hr>";
        $sql = "SELECT * FROM dados";
        $resultado = $conexao->query($sql);
        while ($registro = $resultado->fetch_assoc()) {
          extract($registro);
          $editarId = 'editar'.$id;
          $excluirId = 'excluir'.$id;
          ?>
            <tr>
              <td><?php echo $id;?></td>
              <td><?php echo $nome;?></td>
              <td><?php echo $email;?></td>
              <td><pre><?php echo $msg;?></pre></td>
              <td><?php echo $data;?></td>
              <td>
                <form id="<?php echo $editarId;?>" action="./" method="post">
                    <input type="hidden" name="editar" value="<?php echo $id;?>">
                    <a href="javascript:document.querySelector('#<?php echo $editarId;?>').submit();">
                      Editar
                    </a>
                </form>
              </td>
              <td>
                <form id="<?php echo $excluirId;?>" action="./" method="post">
                  <input type="hidden" name="excluir" value="<?php echo $id;?>">
                  <a href="javascript:document.querySelector('#<?php echo $excluirId;?>').submit();">
                    Excluir
                  </a>
                </form>
              </td>
            </tr>
          <?php
        }
      };
      mysqli_close($conexao);
    ?>
  </tbody>
</table>
<p id="link-voltar"><a href="./">Voltar</a></p>
