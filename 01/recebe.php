<?php
  if (isset($_POST['enviar'])) {
    if (empty($_POST['nome'])) {
      echo "<p>Preencha seu nome.</p>";
    } elseif (empty($_POST['email'])) {
      echo "<p>Preencha seu e-mail.</p>";
    } elseif (empty($_POST['msg'])) {
      echo "<p>Escreva uma mensagem.</p>";
    } else {
      echo "Nome: ". $_POST['nome'] ."<br>";
      echo "E-mail: ". $_POST['email'] ."<br>";
      echo "Mensagem: ". $_POST['msg'] ."<br>";
    };
  } else {
    echo "<p>Você não veio pelo formulário.</p>";
  };
?>
