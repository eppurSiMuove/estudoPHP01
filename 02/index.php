<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <title>Formulário de Contatos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/style.css" rel="stylesheet">
  </head>
  <body>
    <div class="ctn-main">
      <?php
        if (isset($_POST['enviar'])) {
          if (empty($_POST['nome'])) {
            echo "Preencha seu nome.";
          } elseif (empty($_POST['email'])) {
            echo "Preencha seu e-mail.";
          } elseif (empty($_POST['msg'])) {
            echo "Escreva uma mensagem.";
          } else {
            ?>
              <table>
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nome</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Mensagem</th>
                  </tr>
                </thead>
                <tbody>
                  <td>1</td>
                  <td><?php echo $_POST['nome']?></td>
                  <td><?php echo $_POST['email']?></td>
                  <td><pre><?php echo $_POST['msg']?></pre></td>
                </tbody>
              </table>
              <p id="link-voltar"><a href="./">Voltar</a></p>
            <?php
          };
        } else {
          ?>
          <form class="ctn-form" action="./" method="post">
            <h1>Fale conosco</h1>
            <div class="input-field if-text">
              <label for="nome">Nome</label>
              <input type="text" name="nome" id="nome">
            </div>
            <div class="input-field if-text">
              <label for="email">E-mail</label>
              <input type="email" name="email" id="email">
            </div>
            <div class="input-field if-msg">
              <label for="msg">Mensagem:</label>
              <textarea rows="8" cols="80" name="msg" id="msg" spellcheck="false"></textarea></br>
            </div>
            <div class="input-submit">
              <input type="submit" name="enviar" value="Enviar">
            </div>
          </form>
          <?php
        }
      ?>
    </div>
  </body>
</html>
