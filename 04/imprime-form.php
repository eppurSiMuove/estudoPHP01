<?php
  if(isset($_POST['editar'])):
    $conexao = new mysqli("localhost", "Admin", "123456", "estudos01");
    if ($conexao->connect_error) :
      echo "something went wrong x.x";
    else :
      $titulo = 'Editar mensagem';
      $btnName = 'atualizar';
      $btnValue = 'Atualizar';
      $id = $_POST['editar'];
      $sql = "SELECT nome, email, msg FROM dados WHERE id = {$id}";
      $resultado = $conexao->query($sql);
      $registro = $resultado->fetch_assoc();
      extract($registro);
    endif;
    mysqli_close($conexao);
  else :
    $titulo = 'Fale conosco';
    $btnName = 'enviar';
    $btnValue = 'Enviar';
    $nome = '';
    $email = '';
    $msg = '';
    $id = '0';
  endif;
?>
<form class="ctn-form" action="./" method="post">
<h1><?php echo $titulo;?></h1>
  <input type="hidden" name="id" value="<?php echo $id;?>">
  <div class="input-field if-text">
    <label for="nome">Nome</label>
    <input type="text" name="nome" id="nome" value="<?php echo $nome;?>">
  </div>
  <div class="input-field if-text">
    <label for="email">E-mail</label>
    <input type="email" name="email" id="email" value="<?php echo $email;?>">
  </div>
  <div class="input-field if-msg">
    <label for="msg">Mensagem:</label>
    <textarea rows="8" cols="80" name="msg" id="msg" spellcheck="false"><?php echo $msg;?></textarea></br>
  </div>
  <div class="input-submit">
    <input type="submit" name="<?php echo $btnName;?>" value="<?php echo $btnValue;?>">
  </div>
</form>
