<?php
  require_once("head.php");
  session_start();

  if (isset($_GET['deslogar'])):
    unset($_SESSION);
    session_destroy();
    header("location: login.php");
  endif;

  if (!isset($_SESSION['form-contato'])):
    header("location: login.php");
  endif;

  echo "<a href='?deslogar'>Deslogar</a>";
  if (isset($_POST['excluir'])) :
    require_once("excluir.php");
    require_once("imprime-tabela.php");

  elseif (isset($_POST['enviar']) || isset($_POST['atualizar'])) :
    require_once("gravar.php");
    require_once("imprime-tabela.php");

  else :
    require_once("imprime-form.php");

  endif;
  require_once("eof.php");
?>
