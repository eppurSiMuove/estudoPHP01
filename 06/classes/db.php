<?php
  class DB {
    public function connect() {
      try {
        if (!isset($pdo)) {
          $pdo = new PDO("mysql:host=localhost;dbname=estudos01", "Admin", "123456");
          $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $pdo->exec("SET CHARACTER SET utf8");
          return $pdo;
        }
      } catch (PDOException $err) {
        echo "$err <br>";
        die("<h1>Erro de conexão, contate o Administrador do sistema.</h1>");
      }
    }
  }
