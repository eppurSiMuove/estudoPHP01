<?php
  require_once("db.php");

  class Table extends DB {
    public function selectAll() {
      $sql = "SELECT * FROM dados";
      $result = self::connect()->query($sql, PDO::FETCH_ASSOC);
      return $result->fetchAll();
    }

    public function selectById(int $identifier) {
      $sql = "SELECT * FROM dados WHERE id = {$identifier}";
      $result = self::connect()->query($sql, PDO::FETCH_ASSOC);
      return $result->fetchAll();
    }

    public function insertData(Array $post) {
      unset($post['enviar']);
      if(in_array(false, $post)) :
        return false;
      endif;

      extract($post);
      $sql = "INSERT INTO dados SET nome = ?, email = ?, senha = ?, msg = ?, data = NOW()";
      $result = self::connect()->prepare($sql);
      $result->execute(array($nome, $email, $senha, $msg));
      if ($result->rowCount() == 1) :
        return true;
      else :
        return false;
      endif;
    }

    public function updateData(int $identifier, Array $post) {
      if(in_array(false, $post)) :
        return false;
      endif;

      extract($post);
      $sql = "UPDATE dados SET nome = ?, email = ?, senha = ?, msg = ?, data = NOW() WHERE id = {$identifier}";
      $result = self::connect()->prepare($sql);
      $result->execute(array($nome, $email, $senha, $msg));
      if ($result->rowCount() == 1) :
        return true;
      else :
        return false;
      endif;
    }
  }
