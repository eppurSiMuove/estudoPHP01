<table>
  <thead>
    <tr>
      <th scope="col" class="col-id">ID</th>
      <th scope="col" class="col-nome">Nome</th>
      <th scope="col" class="col-email">E-mail</th>
      <th scope="col" class="col-senha">Senha</th>
      <th scope="col" class="col-msg">Mensagem</th>
      <th scope="col" class="col-data">Data</th>
      <th colspan="2">Ações</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $rows = $oTable->selectAll();
      foreach ($rows as $row) :
        extract($row);
        $editarId = 'editar'.$id;
        $excluirId = 'excluir'.$id;
        ?>
          <tr>
            <td class="col-id"><?= $id ?></td>
            <td class="col-nome"><?= $nome ?></td>
            <td class="col-email"><?= $email ?></td>
            <td class="col-senha"><?= $senha ?></td>
            <td class="col-msg"><pre><?= $msg ?></pre></td>
            <td class="col-data"><?= date("d/m/Y H:i:s", strtotime($data)) ?></td>
            <td>
              <form id="<?= $editarId ?>" action="./" method="post">
                  <input type="hidden" name="editar" value="<?= $id ?>">
                  <a href="javascript:document.querySelector('#<?= $editarId ?>').submit();">
                    Editar
                  </a>
              </form>
            </td>
            <td>
              <form id="<?= $excluirId ?>" action="./" method="post">
                <input type="hidden" name="excluir" value="<?= $id ?>">
                <a href="javascript:document.querySelector('#<?= $excluirId ?>').submit();">
                  Excluir
                </a>
              </form>
            </td>
          </tr>
        <?php
      endforeach
    ?>
  </tbody>
</table>
<p id="link-voltar"><a href="./">Voltar</a></p>
