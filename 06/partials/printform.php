<?php
  if(isset($_POST['editar'])) :
    $titulo = 'Editar mensagem';
    $btnName = 'atualizar';
    $btnValue = 'Atualizar';
    $id = $_POST['editar'];
    $row = $oTable->selectById($id);
    extract($row[0]);
  else :
    $titulo = 'Fale conosco';
    $btnName = 'enviar';
    $btnValue = 'Enviar';
    $nome = '';
    $email = '';
    $senha = '';
    $msg = '';
    if (isset($_POST['enviar'])) :
      $id = 0;
      if($oTable->insertData($_POST)) :
        $qryResult = "Dados inseridos com sucesso.";
      else :
        $qryResult = "Erro ao inserir dados.";
      endif;
    elseif (isset($_POST['atualizar'])) :
      if($oTable->updateData((int)$_POST['id'], $_POST)) :
        $qryResult = "Dados atualizados com sucesso.";
      else :
        $qryResult = "Erro ao atualizar dados.";
      endif;
    endif;
  endif;
?>
<form class="ctn-form" action="./" method="post">
  <h1><?= $titulo ?></h1>
  <?php if(isset($qryResult)): ?>
    <p class="alert-red"><?= $qryResult ?></p>
  <?php endif; ?>
  <input type="hidden" name="id" value="<?= $id ?>">
  <div class="input-field">
    <label for="nome">Nome</label>
    <input type="text" name="nome" id="nome" value="<?= $nome ?>" required>
  </div>
  <div class="input-field">
    <label for="email">E-mail</label>
    <input type="email" name="email" id="email" value="<?= $email ?>" required>
  </div>
  <div class="input-field">
    <label for="senha">Senha</label>
    <input type="password" name="senha" id="senha" value="<?= $senha ?>" required>
  </div>
  <div class="input-field">
    <label for="msg">Mensagem:</label>
    <textarea rows="8" cols="80" name="msg" id="msg" spellcheck="false" required><?= $msg ?></textarea></br>
  </div>
  <div class="input-submit">
    <input type="submit" name="<?= $btnName ?>" value="<?= $btnValue ?>">
  </div>
</form>
